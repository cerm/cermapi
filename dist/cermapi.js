/**
 * Created by Anders on 01/07/16.
 * version 1.3.0
 */
function AbstractInterface() {

    //Private vars
    // var factor = 1;
    var impl;
    var production = false;

    //Enable caching in production
    try {
        window.parent.$.ajaxSetup({
            cache: production
        });
    } catch (e) {}

    //Private setup method
    var setup = function () {
        void 0;
        if (window.parent.cpAPIInterface) {
            //CAPTIVATE
            impl = new Captivate9Interface();
            void 0;
        } else if (typeof window.parent.GetPlayer !== 'undefined') {
            //STORYLINE
            impl = new StorylineInterface();
            void 0;
        } else {
            impl = new WebInterface();
            void 0;
        }
    };


    /**
     * Set raw score of user
     * @param      {Number}   score score to set
     * @param      {Number}   maxScore maximum possible score (normally 100)
     */
    this.setRawScore = function (score, maxScore, minScore) {
    };

    /**
     * Set weighting factor
     * @param      {Number}   weight weighting factor
     */
    this.setScoreWeighting = function (weight) {
    };

    /**
     * Get student ID
     * @param
     * @return     {Number} Student ID (null if not found)
     */
    this.getLearnerID = function () {
        return impl.getLearnerID();
    };

    /**
     * Get student score
     * @param
     * @return     {Number} Student ID
     */
    this.getScore = function () {

    };

    /**
     * Test to see if connection to LMS is established
     * @param
     * @return     {Boolean}
     */
    this.testConnection = function () {

    };

    /**
     * Get LMS value
     * @param      {String} key key of value to find
     * @return     {String} LMS value corresponding to the given key
     */
    this.getValue = function (str) {

    };

    /**
     * Jump to specific slide
     * @param      {Number} index index of slide to jump to (first slide = 1)
     */
    this.jumpToSlide = function (index) {
        void 0;
        impl.jumpToSlide(index);
    };

    /**
     * Scrub to specific MS relative to current location
     * @param      {Number} ms Seeks to a particular time (in ms) in in relation to the playheads placement (can be negative)
     */
    this.scrubInMS = function (ms) {
        void 0;
        impl.scrubInMS(ms);
    };

    /**
     * Returns specific variable value
     * @param      {String} name variable name
     * @return     {String} variable value
     */
    this.getVariable = function (name) {
        void 0;
        return impl.getVariable(name);
    };

    /**
     * Sets specific variable value
     * @param      {String} name variable name
     * @param      {String} value variable value
     */
    this.setVariable = function (name, value) {
        void 0;
        return impl.setVariable(name, value);
    };

    /**
     * Continues to play current slide
     */
    this.playSlide = function () {
        impl.playSlide();
    };

    /**
     * Sets weighting of non-symtris elements in project and adds value(s) contained in vNameArr.
     * @param      {Number} fac factor to weight all non-symtris (quiz)elements.
     * @param      {Number} vNameArr name of variable(s) holding the current value to add to the automatically (by e.g. Captivate) generated score. Can be an array, if more than one symtris-element is needed.
     */
    this.editScoring = function (fac, vNameArr) {
        impl.editScoring(fac, vNameArr);
    };

    setup();

}
/**
 * Created by Anders on 16/03/16.
 * version 1.1.1
 */

function Captivate9Interface() {

    var factor = 1;
    var ENABEL_REPORTING = false;

    // /**
    //  * Interception for point-based setting of score. Multiplies the reported score with the score weighting factor
    //  */
    // if(typeof objLMS !== 'undefined'){
    //     var _SetPointBasedScore = objLMS.SetPointBasedScore;
    //     objLMS.SetPointBasedScore = function (score, maxScore) {
    //         console.log('intercepted for: ' + score + ', ' + maxScore + ', factor: ' + factor);
    //         _SetPointBasedScore(score * factor, maxScore);
    //     };
    // }


    //START google sheets reporting
    function setupReporting() {
        if ( typeof window.cpAPIEventEmitter !== 'undefined') {

            // console.log('cpAPIEventEmitter found');
            // console.log(window.cpAPIEventEmitter);
            //TODO report 'course opened' to sheets
            if (!window.sessionStorage.getItem('ID')) {
                window.sessionStorage.setItem("ID", (new Date()).getTime());
                var eventData = {
                    Name: 'COURSE_OPEN',
                    Data: {slideNumber: window.cpAPIInterface.getCurrentSlideIndex()},
                    timeStamp: 0
                };
                postData(createPostData(eventData));

                window.addEventListener("beforeunload", function () {
                    var eventData = {
                        Name: 'COURSE_CLOSE',
                        Data: {slideNumber: window.cpAPIInterface.getCurrentSlideIndex()},
                        timeStamp: 'unknown'
                    };
                    postData(createPostData(eventData));
                });
            }

            if (!window.cpAPIEventEmitter.callbackFns.CPAPI_SLIDEENTER) {
                window.cpAPIEventEmitter.addEventListener("CPAPI_SLIDEENTER", function (event) {
                    void 0;
                    void 0;
                    postData(createPostData(event));
                });
                window.cpAPIEventEmitter.addEventListener("CPAPI_SLIDEEXIT", function (event) {
                    void 0;
                    void 0;
                    postData(createPostData(event));
                });
                window.cpAPIEventEmitter.addEventListener("CPAPI_QUESTIONSUBMIT", function (event) {
                    void 0;
                    void 0;
                    postData(createPostData(event));
                });
            }
        }

    }

    var createPostData = function (eventData) {
        var eventType;
        switch (eventData.Name) {
            case 'CPAPI_SLIDEENTER':
                eventType = 'SLIDE_ENTER';
                break;
            case 'CPAPI_SLIDEEXIT':
                eventType = 'SLIDE_EXIT';
                break;
            case 'CPAPI_QUESTIONSUBMIT':
                eventType = 'QUESTION_SUBMIT';
                break;
            case 'COURSE_OPEN':
                eventType = 'COURSE_OPEN';
                break;
            case 'COURSE_CLOSE':
                eventType = 'COURSE_CLOSE';
                break;
        }
        var result = 'learnerID=' + getLearnerIDImpl()
            + '&slideType=' + eventData.Data.st
            + '&timeStamp=' + new Date()
            + '&timeSinceCourseOpen=' + eventData.timeStamp
            + '&courseID=' + getVariableImpl('courseID')
            + '&sessionID=' + window.sessionStorage.getItem('ID')
            + '&eventType=' + eventType;

        if (eventType == 'QUESTION_SUBMIT') {
            result += '&questionAnsweredCorrectly=' + eventData.Data.questionAnsweredCorrectly
                + '&questionAttempts=' + eventData.Data.questionAttempts
                + '&selectedAnswer=' + eventData.Data.selectedAnswer
                + '&slideNumber=' + (Number(eventData.Data.slideNumber) + 1);
        } else {
            result += '&slideNumber=' + eventData.Data.slideNumber;
        }

        void 0;

        return result;
    };

    var postData = function (serializedData) {
        $.ajax({
            url: 'https://script.google.com/macros/s/AKfycbyk73n4bvKnpad-mKSVAFN2abQqN7fDeD7ChriXxsYUGHApdJM7/exec',
            type: 'post',
            data: serializedData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            success: function (response) {
                void 0;
                // console.log(response.data.qSeries);
                void 0;
            }
        });
    };
    //END google sheets reporting


    function setRawScore(score, maxScore, minScore) {
        _SetPointBasedScore(score, maxScore);
    }


    var setScoreWeighting = function (weight) {
        this.factor = weight;
    };

    var getLearnerIDImpl = function () {
        if (typeof objLMS !== 'undefined') {
            var tmp = objLMS.GetStudentID();
            void 0;
            return tmp;
        } else {
            return null;
        }
    };


    this.getLearnerID = function () {
        return getLearnerIDImpl();
    };


    var getScore = function () {
        if (typeof objLMS !== 'undefined') {
            var tmp = objLMS.GetScore();
            void 0;
            return tmp;
        }

    };


    var testConnection = function () {
        void 0;
        return true;
    };


    var getValue = function (str) {
        if (SCORM2004_CallGetValue) {
            return SCORM2004_CallGetValue(str);
        } else {
            return SCORM_CallLMSGetValue(str);
        }
    };

    this.scrubInMS = function (deltaMS) {
        var currentFrame = window.cpAPIInterface.getCurrentFrame();
        var fps = window.cpAPIInterface.getPlaySpeed();
        var currentMS = Math.ceil(currentFrame * 1000 / fps);
        window.cpAPIInterface.navigateToTime(currentMS + deltaMS);
    };


    this.jumpToSlide = function (index) {
        void 0;
        // cpAPIInterface.gotoSlide(index - 1);
        window.cpAPIInterface.gotoSlide(index - 1);
    };

    var getVariableImpl = function (name) {
        void 0;
        // console.log(window[name]);
        void 0;
        // return window[name];
        return window.cpAPIInterface.getVariableValue(name);
    };

    this.getVariable = function (name) {
        void 0;
        return getVariableImpl(name);
    };

    this.setVariable = function (name, value) {
        void 0;
        // window[name] = value;
        window.cpAPIInterface.setVariableValue(name, value);
    };

    this.playSlide = function () {
        void 0;
        window.cpAPIInterface.play();
    };

    this.editScoring = function (fac, vNameArr) {
        //TODO create this method
    };

    if (typeof getVariableImpl('courseID') !== 'undefined' && ENABEL_REPORTING) {
        setupReporting();
    }
}

/**
 * Created by Anders on 04/03/16.
 * version 1.1.1
 */

function StorylineInterface() {

    /**
     * Score weighting factor
     */
    var factor = 1;

    /**
     * Edit point reporting
     */
    var editPoints = false;

    /**
     * Private vars
     */
    var prevScore = 0;
    var player;
    //var variableName;
    var variableNameArr;
    var FIRST_RUN = true;
    var context = window.parent;

    /**
     * WARNING: METHOD MOST LIKELY DEPRECATED. TEST NEEDED!
     */
    this.editScoring = function (fac, vNameArr) {
        try {
            editPoints = true;
            factor = fac;
            player = context.GetPlayer();
            //variableName = vName;
            variableNameArr = vNameArr;
            //prevScore = Number(player.GetVar(variableName));

            //Untested idea to only add one eventlistener, thus only reporting once (and on 1x number of slides) (08.05.15):
            if (FIRST_RUN) {
                window.addEventListener('beforeunload', function (event) {
                    void 0;
                    ReportStatus();
                    var a = context.story.toResumeData();
                    context.objLMS.SetDataChunk(a);
                });

                FIRST_RUN = false;
                if (context.GetEntryMode() == 3) {
                    context.g_oContentResults.nScore = context.g_oContentResults.nScore * factor;
                }
            }

            //Original working reporting (08.05.15):
            // if(GetEntryMode() == 3 && FIRST_RUN){
            //    g_oContentResults.nScore = g_oContentResults.nScore * factor;
            //    FIRST_RUN = false;
            // }
            // window.addEventListener('unload', function(event) {
            //    console.log('lms.js onunload');
            //    ReportStatus();
            // });

        } catch (err) {
            void 0;
        }

    };

    function stopEditScore() {
        editPoints = false;
    }

// /**
//  * Interception for score setting. Multiplies the reported score with the score weighting factor (default = 1)
//  */
// var _SetScore = SetScore;
// SetScore = function(intScore, intMaxScore, intMinScore){
//     console.log('intercepted for: ' + intScore + '/' + intMaxScore + ', intMinScore: ' + intMinScore + ', factor: ' + factor);
//     _SetScore(intScore * factor, intMaxScore, intMinScore);
// };

    /**
     * Overwrite of ReportStatus() method
     * WARNING: METHOD MOST LIKELY DEPRECATED. TEST NEEDED!
     */
    ReportStatus = function () {
        void 0;
        if (context.g_oContentResults.strType == "quiz") {
            //Start Anders' edit
            if (editPoints) {
                try {
                    context.g_oContentResults.nScore = context.g_oContentResults.nScore * factor;
                    //make the vName variable into an array and loop through it here, adding up the 'tmp' variable
                    var tmp = 0;
                    for (var i in variableNameArr) {
                        tmp += Number(player.GetVar(variableNameArr[i]));
                    }
                    context.g_oContentResults.nScore += tmp;
                } catch (err) {
                    void 0;
                }

            }
            //End Anders' edit
            context.lmsAPI.SetScore(g_oContentResults.nScore, 100, 0);
        }

        context.SetStatus(normalizeStatus(g_oContentResults.strStatus));
    };

    /**
     * Interception for RecordInteraction(arrArgs) method
     */
    if (typeof context.RecordInteraction !== 'undefined') {
        var _RecordInteraction = context.RecordInteraction;
        context.RecordInteraction = function (arrArgs) {
            void 0;
            if (editPoints) {
                try {
                    arrArgs[8] = arrArgs[8] * factor;
                    for (var i in variableNameArr) {
                        arrArgs[8] += Number(player.GetVar(variableNameArr[i]));
                    }
                    arrArgs[8] = arrArgs[8].toString();
                } catch (err) {
                    void 0;
                }
            }
            _RecordInteraction(arrArgs);
        };
    }


    /**
     * Overwrite of LMSUnload() method
     */
    if (typeof context.LMSUnload !== 'undefined') {
        var _LMSUnload = context.LMSUnload;
        context.LMSUnload = function () {
            void 0;
            if (context.g_bAPIPresent) {
                //Start Anders' edit
                if (editPoints) {
                    editPointFactor = 1;
                }
                //Start Anders' edit
                ReportStatus();

                lmsAPI.Unload();
            }
            _LMSUnload();
        };
    }


    function setRawScore(score, maxScore, minScore) {
        void 0;
        _SetScore(score, maxScore, minScore);
    }


    function addPoints(points) {
        void 0;
        void 0;
        context.g_oContentResults.nScore += points;
    }


    function setScoreWeighting(weight) {
        void 0;
        this.factor = weight;
    }


    //TODO test this
    this.getLearnerID = function () {
        return context.GetStudentID();
    };


    function getScore() {
        return context.GetScore();
    }


    function testConnection() {
    }


    this.getVariable = function (name) {
        var returnVal = context.GetPlayer().GetVar(name);
        void 0;
        void 0;
        return returnVal;
    };

    this.setVariable = function (key, val) {
        void 0;
        context.GetPlayer().SetVar(key, val);
        // var newDataChunk = story.toResumeData();
        // objLMS.SetDataChunk(newDataChunk);
    };

    this.scrubInMS = function (ms) {
        //TODO finish this method
    };

    this.jumpToSlide = function (index) {
        void 0;
        var target = parseInt(index) + 4;
        // var target = 11;
        context.GetPlayer().showSlideIndex(target, null, true);
    };

    this.playSlide = function () {
        void 0;
        //TODO finish this
    };
}
/**
 * Created by Anders on 17/11/2016.
 */
function WebInterface() {

    this.getVariable = function (name) {
        var win = $(window);
        var returnVal;
        if (window.frameElement) {
            returnVal = window.frameElement.getAttribute(name);
        } else {
            returnVal = 0;
            void 0;
        }
        //console.log('getVariable: ' + name + ', val: ' + returnVal);
        //console.log(returnVal);
        return returnVal;
    };

    this.setVariable = function (key, val) {
        void 0;

        if (window.frameElement) {
            window.frameElement.setAttribute(key, val);
        } else {
            void 0;
        }
    };
}