/**
 * Created by Anders on 01/07/16.
 * version 1.3.0
 */
function AbstractInterface() {

    //Private vars
    // var factor = 1;
    var impl;
    var production = false;

    //Enable caching in production
    try {
        window.parent.$.ajaxSetup({
            cache: production
        });
    } catch (e) {}

    //Private setup method
    var setup = function () {
        console.log('setup');
        if (window.parent.cpAPIInterface) {
            //CAPTIVATE
            impl = new Captivate9Interface();
            console.log(impl);
        } else if (typeof window.parent.GetPlayer !== 'undefined') {
            //STORYLINE
            impl = new StorylineInterface();
            console.log(impl);
        } else {
            impl = new WebInterface();
            console.log(impl);
        }
    };


    /**
     * Set raw score of user
     * @param      {Number}   score score to set
     * @param      {Number}   maxScore maximum possible score (normally 100)
     */
    this.setRawScore = function (score, maxScore, minScore) {
    };

    /**
     * Set weighting factor
     * @param      {Number}   weight weighting factor
     */
    this.setScoreWeighting = function (weight) {
    };

    /**
     * Get student ID
     * @param
     * @return     {Number} Student ID (null if not found)
     */
    this.getLearnerID = function () {
        return impl.getLearnerID();
    };

    /**
     * Get student score
     * @param
     * @return     {Number} Student ID
     */
    this.getScore = function () {

    };

    /**
     * Test to see if connection to LMS is established
     * @param
     * @return     {Boolean}
     */
    this.testConnection = function () {

    };

    /**
     * Get LMS value
     * @param      {String} key key of value to find
     * @return     {String} LMS value corresponding to the given key
     */
    this.getValue = function (str) {

    };

    /**
     * Jump to specific slide
     * @param      {Number} index index of slide to jump to (first slide = 1)
     */
    this.jumpToSlide = function (index) {
        console.log('jumpToSlide: ' + index);
        impl.jumpToSlide(index);
    };

    /**
     * Scrub to specific MS relative to current location
     * @param      {Number} ms Seeks to a particular time (in ms) in in relation to the playheads placement (can be negative)
     */
    this.scrubInMS = function (ms) {
        console.log('scrubInMS: ' + ms);
        impl.scrubInMS(ms);
    };

    /**
     * Returns specific variable value
     * @param      {String} name variable name
     * @return     {String} variable value
     */
    this.getVariable = function (name) {
        console.log('getVariable: ' + name);
        return impl.getVariable(name);
    };

    /**
     * Sets specific variable value
     * @param      {String} name variable name
     * @param      {String} value variable value
     */
    this.setVariable = function (name, value) {
        console.log('setVariable: ' + name + ', value ' + value);
        return impl.setVariable(name, value);
    };

    /**
     * Continues to play current slide
     */
    this.playSlide = function () {
        impl.playSlide();
    };

    /**
     * Sets weighting of non-symtris elements in project and adds value(s) contained in vNameArr.
     * @param      {Number} fac factor to weight all non-symtris (quiz)elements.
     * @param      {Number} vNameArr name of variable(s) holding the current value to add to the automatically (by e.g. Captivate) generated score. Can be an array, if more than one symtris-element is needed.
     */
    this.editScoring = function (fac, vNameArr) {
        impl.editScoring(fac, vNameArr);
    };

    setup();

}