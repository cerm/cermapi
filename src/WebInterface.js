/**
 * Created by Anders on 17/11/2016.
 */
function WebInterface() {

    this.getVariable = function (name) {
        var win = $(window);
        var returnVal;
        if (window.frameElement) {
            returnVal = window.frameElement.getAttribute(name);
        } else {
            returnVal = null;
            console.warn('Cannot get variable, window.frameElement not available');
        }
        //console.log('getVariable: ' + name + ', val: ' + returnVal);
        //console.log(returnVal);
        return returnVal;
    };

    this.setVariable = function (key, val) {
        console.log('setVariable: ' + key + ', val: ' + val);

        if (window.frameElement) {
            window.frameElement.setAttribute(key, val);
        } else {
            console.warn('Cannot set variable, window.frameElement not available');
        }
    };
}